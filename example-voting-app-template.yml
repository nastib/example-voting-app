apiVersion: v1
kind: Template
metadata:
  creationTimestamp: null
  name: example-voting-app-template
objects:
- apiVersion: v1
  kind: Secret
  metadata:
    name: db
  data:
    database-name: db
    database-password: postgres_password
    database-user: postgres_user

- apiVersion: v1
  kind: Secret
  metadata:
    name: redis
  data:
    database-password: redis_password

- apiVersion: build.openshift.io/v1
  kind: BuildConfig
  metadata:
    name: result
  spec:
    output:
      to:
        kind: ImageStreamTag
        name: 'result:latest'
    runPolicy: Serial
    source:
      contextDir: /result
      git:
        ref: master
        uri: 'https://gitlab.com/nastib/example-voting-app.git'
      type: Git
    strategy:
      sourceStrategy:
        env:
          - name: PORT
            value: '8080'
        from:
          kind: ImageStreamTag
          name: 'nodejs:8'
          namespace: openshift
      type: Source
    triggers:
      - generic:
          secret: 9bf24107390fa5db
        type: Generic
      - github:
          secret: 250902c16878c75a
        type: GitHub
      - imageChange:
          lastTriggeredImageID: >-
            docker.io/centos/nodejs-8-centos7@sha256:588205ed23e784354107a82ec10056aeb6cb223725a8b4d4643aa414b139db19
        type: ImageChange
      - type: ConfigChange
  status:
    lastVersion: 1

- apiVersion: build.openshift.io/v1
  kind: BuildConfig
  metadata:
    name: vote
  spec:
    output:
      to:
        kind: ImageStreamTag
        name: 'vote:latest'
    runPolicy: Serial
    source:
      contextDir: /vote
      git:
        ref: master
        uri: 'https://gitlab.com/nastib/example-voting-app.git'
      type: Git
    strategy:
      sourceStrategy:
        from:
          kind: ImageStreamTag
          name: 'python:3.6'
          namespace: openshift
      type: Source
    triggers:
      - generic:
          secret: 3fde6cbb202a6ac2
        type: Generic
      - github:
          secret: 292901cf15775b9e
        type: GitHub
      - imageChange:
          lastTriggeredImageID: >-
            docker.io/centos/python-36-centos7@sha256:d10c46b6db436357965a96716e9f5d230d9b1a58c6db1f0c4f43c1fb1994cd79
        type: ImageChange
      - type: ConfigChange
  status:
  lastVersion: 1

- apiVersion: build.openshift.io/v1
  kind: BuildConfig
  metadata:
    name: worker
  spec:
    output:
      to:
        kind: ImageStreamTag
        name: 'worker:latest'
    runPolicy: Serial
    source:
      contextDir: /worker
      git:
        ref: master
        uri: 'https://gitlab.com/nastib/example-voting-app.git'
      type: Git
    strategy:
      dockerStrategy: {}
      type: Docker
    triggers:
      - generic:
          secret: c2e8f09242b94a14
        type: Generic
      - github:
          secret: 278ad97b95d1eb8a
        type: GitHub
      - type: ConfigChange
  status:
    lastVersion: 2 

- apiVersion: image.openshift.io/v1
  kind: ImageStream
  metadata:
    name: vote
  spec:
    lookupPolicy:
      local: false

- apiVersion: image.openshift.io/v1
  kind: ImageStream
  metadata:
    name: worker
  spec:
    lookupPolicy:
      local: false

- apiVersion: image.openshift.io/v1
  kind: ImageStream
  metadata:
    name: result
  spec:
    lookupPolicy:
      local: false

- apiVersion: apps.openshift.io/v1
  kind: DeploymentConfig
  metadata:
    namespace: voting-application
  spec:
    replicas: 1
    selector:
      name: db
    strategy:
      activeDeadlineSeconds: 21600
      recreateParams:
        timeoutSeconds: 600
      resources: {}
      type: Recreate
    template:
      metadata:
        creationTimestamp: null
        labels:
          name: db
      spec:
        containers:
          - env:
              - name: POSTGRESQL_USER
                valueFrom:
                  secretKeyRef:
                    key: database-user
                    name: db
              - name: POSTGRESQL_PASSWORD
                valueFrom:
                  secretKeyRef:
                    key: database-password
                    name: db
              - name: POSTGRESQL_DATABASE
                valueFrom:
                  secretKeyRef:
                    key: database-name
                    name: db
            image: >-
              docker.io/centos/postgresql-96-centos7@sha256:c78aadb5114b2cac6ae65177272ec417c10142395fea03eb5c73f77220fd5e85
            imagePullPolicy: IfNotPresent
            livenessProbe:
              exec:
                command:
                  - /bin/sh
                  - '-i'
                  - '-c'
                  - pg_isready -h 127.0.0.1 -p 5432
              failureThreshold: 3
              initialDelaySeconds: 30
              periodSeconds: 10
              successThreshold: 1
              timeoutSeconds: 1
            name: postgresql
            ports:
              - containerPort: 5432
                protocol: TCP
            readinessProbe:
              exec:
                command:
                  - /bin/sh
                  - '-i'
                  - '-c'
                  - >-
                    psql -h 127.0.0.1 -U $POSTGRESQL_USER -q -d
                    $POSTGRESQL_DATABASE -c 'SELECT 1'
              failureThreshold: 3
              initialDelaySeconds: 5
              periodSeconds: 10
              successThreshold: 1
              timeoutSeconds: 1
            resources:
              limits:
                memory: 100Mi
            securityContext:
              capabilities: {}
              privileged: false
            terminationMessagePath: /dev/termination-log
            terminationMessagePolicy: File
            volumeMounts:
              - mountPath: /var/lib/pgsql/data
                name: db-data
        dnsPolicy: ClusterFirst
        restartPolicy: Always
        schedulerName: default-scheduler
        securityContext: {}
        terminationGracePeriodSeconds: 30
        volumes:
          - name: db-data
            persistentVolumeClaim:
              claimName: db
    test: false
    triggers:
      - imageChangeParams:
          automatic: true
          containerNames:
            - postgresql
          from:
            kind: ImageStreamTag
            name: 'postgresql:9.6'
            namespace: openshift
          lastTriggeredImage: >-
            docker.io/centos/postgresql-96-centos7@sha256:c78aadb5114b2cac6ae65177272ec417c10142395fea03eb5c73f77220fd5e85
        type: ImageChange
      - type: ConfigChange

- apiVersion: apps.openshift.io/v1
  kind: DeploymentConfig
  metadata:
    name: redis
  spec:
    replicas: 1
    selector:
      name: redis
    strategy:
      activeDeadlineSeconds: 21600
      recreateParams:
        timeoutSeconds: 600
      resources: {}
      type: Recreate
    template:
      metadata:
        creationTimestamp: null
        labels:
          name: redis
      spec:
        containers:
          - env:
              - name: REDIS_PASSWORD
                valueFrom:
                  secretKeyRef:
                    key: database-password
                    name: redis
            image: 
            imagePullPolicy: IfNotPresent
            livenessProbe:
              failureThreshold: 3
              initialDelaySeconds: 30
              periodSeconds: 10
              successThreshold: 1
              tcpSocket:
                port: 6379
              timeoutSeconds: 1
            name: redis
            ports:
              - containerPort: 6379
                protocol: TCP
            readinessProbe:
              exec:
                command:
                  - /bin/sh
                  - '-i'
                  - '-c'
                  - >-
                    test "$(redis-cli -h 127.0.0.1 -a $REDIS_PASSWORD ping)" ==
                    "PONG"
              failureThreshold: 3
              initialDelaySeconds: 5
              periodSeconds: 10
              successThreshold: 1
              timeoutSeconds: 1
            resources:
              limits:
                memory: 512Mi
            securityContext:
              capabilities: {}
              privileged: false
            terminationMessagePath: /dev/termination-log
            terminationMessagePolicy: File
            volumeMounts:
              - mountPath: /var/lib/redis/data
                name: redis-data
        dnsPolicy: ClusterFirst
        restartPolicy: Always
        schedulerName: default-scheduler
        securityContext: {}
        terminationGracePeriodSeconds: 30
        volumes:
          - emptyDir: {}
            name: redis-data
    test: false
    triggers:
      - imageChangeParams:
          automatic: true
          containerNames:
            - redis
          from:
            kind: ImageStreamTag
            name: 'redis:3.2'
            namespace: openshift
          lastTriggeredImage: 
        type: ImageChange
      - type: ConfigChange

- apiVersion: apps.openshift.io/v1
  kind: DeploymentConfig
  metadata:
    name: result
  spec:
    replicas: 1
    selector:
      deploymentconfig: result
    strategy:
      activeDeadlineSeconds: 21600
      resources: {}
      rollingParams:
        intervalSeconds: 1
        maxSurge: 25%
        maxUnavailable: 25%
        timeoutSeconds: 600
        updatePeriodSeconds: 1
      type: Rolling
    template:
      metadata:
        creationTimestamp: null
        labels:
          app: result
          deploymentconfig: result
      spec:
        containers:
          - image: 
            imagePullPolicy: Always
            name: result
            ports:
              - containerPort: 8080
                protocol: TCP
            resources: {}
            terminationMessagePath: /dev/termination-log
            terminationMessagePolicy: File
        dnsPolicy: ClusterFirst
        restartPolicy: Always
        schedulerName: default-scheduler
        securityContext: {}
        terminationGracePeriodSeconds: 30
    test: false
    triggers:
      - imageChangeParams:
          automatic: true
          containerNames:
            - result
          from:
            kind: ImageStreamTag
            name: 'result:latest'
            namespace: voting-application
          lastTriggeredImage: 
        type: ImageChange
      - type: ConfigChange

- apiVersion: apps.openshift.io/v1
  kind: DeploymentConfig
  metadata:
    name: vote
  spec:
    replicas: 1
    selector:
      deploymentconfig: vote
    strategy:
      activeDeadlineSeconds: 21600
      resources: {}
      rollingParams:
        intervalSeconds: 1
        maxSurge: 25%
        maxUnavailable: 25%
        timeoutSeconds: 600
        updatePeriodSeconds: 1
      type: Rolling
    template:
      metadata:
        creationTimestamp: null
        labels:
          app: vote
          deploymentconfig: vote
      spec:
        containers:
          - env:
              - name: REDIS_PASSWORD
                value: redis_password
            image: 
            imagePullPolicy: Always
            name: vote
            ports:
              - containerPort: 8080
                protocol: TCP
            resources: {}
            terminationMessagePath: /dev/termination-log
            terminationMessagePolicy: File
        dnsPolicy: ClusterFirst
        restartPolicy: Always
        schedulerName: default-scheduler
        securityContext: {}
        terminationGracePeriodSeconds: 30
    test: false
    triggers:
      - imageChangeParams:
          automatic: true
          containerNames:
            - vote
          from:
            kind: ImageStreamTag
            name: 'vote:latest'
            namespace: voting-application
          lastTriggeredImage: 
        type: ImageChange
      - type: ConfigChange

- apiVersion: apps.openshift.io/v1
  kind: DeploymentConfig
  metadata:
    name: worker
  spec:
    replicas: 1
    selector:
      deploymentconfig: worker
    strategy:
      activeDeadlineSeconds: 21600
      resources: {}
      rollingParams:
        intervalSeconds: 1
        maxSurge: 25%
        maxUnavailable: 25%
        timeoutSeconds: 600
        updatePeriodSeconds: 1
      type: Rolling
    template:
      metadata:
        creationTimestamp: null
        labels:
          app: worker
          deploymentconfig: worker
      spec:
        containers:
          - image: 
            imagePullPolicy: Always
            name: worker
            ports:
              - containerPort: 8080
                protocol: TCP
              - containerPort: 8443
                protocol: TCP
            resources: {}
            terminationMessagePath: /dev/termination-log
            terminationMessagePolicy: File
        dnsPolicy: ClusterFirst
        restartPolicy: Always
        schedulerName: default-scheduler
        securityContext: {}
        terminationGracePeriodSeconds: 30
    test: false
    triggers:
      - imageChangeParams:
          automatic: true
          containerNames:
            - worker
          from:
            kind: ImageStreamTag
            name: 'worker:latest'
            namespace: voting-application
          lastTriggeredImage: 
        type: ImageChange
      - type: ConfigChange

- apiVersion: v1
  kind: Service
  metadata:
    name: worker
  spec:
    ports:
      - name: 8080-tcp
        port: 8080
        protocol: TCP
        targetPort: 8080
      - name: 8443-tcp
        port: 8443
        protocol: TCP
        targetPort: 8443
    selector:
      deploymentconfig: worker
    sessionAffinity: None
    type: ClusterIP
  status:
    loadBalancer: {}

- apiVersion: v1
  kind: Service
  metadata:
    name: result
  spec:
    ports:
      - name: 8080-tcp
        port: 8080
        protocol: TCP
        targetPort: 8080
    selector:
      deploymentconfig: result
    sessionAffinity: None
    type: ClusterIP
  status:
    loadBalancer: {}

- apiVersion: v1
  kind: Service
  metadata:
    name: db
  spec:
    ports:
      - name: postgresql
        port: 5432
        protocol: TCP
        targetPort: 5432
    selector:
      name: db
    sessionAffinity: None
    type: ClusterIP
  status:
    loadBalancer: {}

- apiVersion: v1
  kind: Service
  metadata:
    name: vote
  spec:
    ports:
      - name: 8080-tcp
        port: 8080
        protocol: TCP
        targetPort: 8080
    selector:
      deploymentconfig: vote
    sessionAffinity: None
    type: ClusterIP
  status:
    loadBalancer: {}

- apiVersion: v1
  kind: Service
  metadata:
    name: redis
  spec:
    ports:
      - name: redis
        port: 6379
        protocol: TCP
        targetPort: 6379
    selector:
      name: redis
    sessionAffinity: None
    type: ClusterIP
  status:
    loadBalancer: {}

- apiVersion: route.openshift.io/v1
  kind: Route
  metadata:
    name: worker
  spec:
    host: 
    port:
      targetPort: 8080-tcp
    to:
      kind: Service
      name: worker
      weight: 100
    wildcardPolicy: None
  status:
    ingress:
      - conditions:
          - lastTransitionTime: '2019-09-04T19:19:46Z'
            status: 'True'
            type: Admitted
        host: 
        routerName: router
        wildcardPolicy: None

- apiVersion: route.openshift.io/v1
  kind: Route
  metadata:
    name: result
  spec:
    host: 
    port:
      targetPort: 8080-tcp
    to:
      kind: Service
      name: result
      weight: 100
    wildcardPolicy: None
  status:
    ingress:
      - conditions:
          - lastTransitionTime: '2019-09-04T19:00:36Z'
            status: 'True'
            type: Admitted
        host: 
        routerName: router
        wildcardPolicy: None

- apiVersion: route.openshift.io/v1
  kind: Route
  metadata:
    name: vote
  spec:
    host: 
    port:
      targetPort: 8080-tcp
    to:
      kind: Service
      name: vote
      weight: 100
    wildcardPolicy: None
  status:
    ingress:
      - conditions:
          - lastTransitionTime: '2019-09-04T17:28:19Z'
            status: 'True'
            type: Admitted
        host: 
        routerName: router
        wildcardPolicy: None   
